/**
 * Prefix the image with the API gateway route to get the resized/optimized
 * image.
 */
const getUploadedImageUri = (image, dimensions) => {
  if (!image) return null

  return dimensions ? `/api/upload/${dimensions}/${image}` : `https://${DOMAIN}/uploads/${image}`
}

/**
 * Gets the original uploaded image URI (pre-processed original size)
 */
const getOrigUploadedImageUri = (image) => {
  if (!image) return null

  return `https://${DOMAIN}/uploads-orig/${image}`
}

export {
  getUploadedImageUri,
  getOrigUploadedImageUri
}
