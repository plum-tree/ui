import { toast } from 'react-toastify'
import get from 'lodash.get'
import axios from 'axios'

/**
 * Get the auth token from local storage and decode to get the claims. If a
 * token does not exist in local storage use the API to get it.
 * @return {object} JWT claims object
 */
const getSession = async () => {
  return axios.get('/api/auth/session')
    .then(response => get(response, 'data.token'))
    .catch(() => null)
}

/**
 * Checks if a error given is a axios error with a response code of 403. If so
 * we redirect the user to login and let them know they need to sign in again.
 * @param {Object} error
 * @param {Object} navigate
 * @returns
 */
const loginRequired = (error, navigate) => {
  if (get(error, 'response.status') === 403) {
    toast.warn('You\'re session expired. Please login again.')
    navigate('/')
    return true
  }
  return false
}

export default {
  getSession,
  loginRequired
}
