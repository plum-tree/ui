import React, { useState, useEffect, useRef } from 'react'
import { Link } from 'react-router-dom'
import styles from './styles.scss'
import auth from '../../common/js/auth'

export default () => {
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = useState(true)
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const wrapperRef = useRef(null)
  const clientId = COGNITO_CLIENT
  const logoutUri = `${window.location.origin}/api/auth/logout`

  useEffect(() => {
    auth.getSession()
      .then(token => setIsLoggedIn(Boolean(token)))
      .finally(() => setLoading(false))

    // Close if clicked on outside of element
    function handleClickOutside (event) {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        setOpen(false)
      }
    }

    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [wrapperRef])

  function handleDropdownToggle () {
    setOpen(!open)
  }

  return (
    <div ref={wrapperRef}>
      <div id='account-dropdown' className={styles.dropdownButton} onClick={handleDropdownToggle}>
        Account
        <i className={open ? `${styles.downArrow} ${styles.downArrowActive}` : styles.downArrow} />
      </div>
      <div className={open ? `${styles.menu} ${styles.menuActive}` : styles.menu}>
        {loading
          ? 'loading...'
          : isLoggedIn
            ? (
              <ul>
                <li><Link to='/account' onClick={handleDropdownToggle}>Account Settings</Link></li>
                <li><a href={`https://auth.plumtree.click/logout?client_id=${clientId}&logout_uri=${logoutUri}`}>Logout</a></li>
              </ul>
              )
            : (
              <ul>
                <li><a href={`/api/auth/launch?returnUrl=${window.location.href}`} rel='nofollow'>Login</a></li>
                <li><a href={`/api/auth/launch?action=signup&returnUrl=${window.location.href}`} rel='nofollow'>Sign up</a></li>
              </ul>
              )}
      </div>
    </div>
  )
}
