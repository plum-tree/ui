import React from 'react'
import { Link } from 'react-router-dom'
import image from './tragic-clown.jpg'

export default () => {
  return (
    <div className='container'>
      <h1 className='sr-only'>Not Found</h1>
      <img
        src={image} style={{
          height: 300,
          display: 'block',
          margin: '30px auto 0px'
        }}
      />
      <p style={{ textAlign: 'center' }}>Looks like this pages does not exist. <Link to='/'>Return home?</Link></p>
    </div>
  )
}
