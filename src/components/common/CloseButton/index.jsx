import React from 'react'
import styles from './styles.scss'

export default ({ onClick, style }) => {
  return (
    <div className={styles.closeButton} onClick={onClick} style={style}>
      <span>Close</span>
      <i className={styles.closeIcon} />
    </div>
  )
}
