import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import get from 'lodash.get'
import { toast } from 'react-toastify'
import Loading from '../Loading'
import auth from '../../common/js/auth'

export default () => {
  const navigate = useNavigate()
  const [loading, setLoading] = useState(true)
  const [token, setToken] = useState(null)

  useEffect(() => {
    axios.get('/api/auth/session')
      .then((response) => {
        setToken(get(response, 'data.token', ''))
      })
      .catch((error) => {
        if (auth.loginRequired(error, navigate)) {
          return
        }
        toast.error('Failed to get account info', { autoClose: false })
      })
      .finally(() => setLoading(false))
  }, [])

  if (loading) {
    return <Loading message='Loading your account details...' />
  }

  return (
    <div className='container'>
      <h1>Account Settings</h1>
      <p>Current session token is:</p>
      <code style={{ wordBreak: 'break-all' }}>{token}</code>
    </div>
  )
}
