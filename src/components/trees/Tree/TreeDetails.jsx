import React from 'react'
import styles from './styles.scss'
import RawHTML from '../../RawHTML'
import { getUploadedImageUri } from '../../../common/js/utils'
import CloseButton from '../../common/CloseButton'

export default ({ title, description, image, style, closeDetails }) => {
  const inlineAvatarStyle = {}
  if (image) {
    inlineAvatarStyle.backgroundImage = `url(${getUploadedImageUri(image, '600x320')})`
  }

  return (
    <div className={styles.treeDetails} style={style}>
      {image && <div className={styles.treeImage} style={inlineAvatarStyle} />}
      <CloseButton onClick={closeDetails} style={{ position: 'absolute', top: 10, right: 10 }} />
      <div className={styles.treeDetailsContent}>
        <h1>{title}</h1>
        <RawHTML html={description} />
      </div>
    </div>
  )
}
