import webpack from 'webpack'
import path from 'path'
import { fileURLToPath } from 'url'
import dotenv from 'dotenv'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { GitRevisionPlugin } from 'git-revision-webpack-plugin'
import FaviconsWebpackPlugin from 'favicons-webpack-plugin'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
dotenv.config({ path: './.env' })
const gitRevisionPlugin = new GitRevisionPlugin()

export default {
  // clean up output to be less noisy
  stats: {
    assets: true,
    children: false
  },
  // Asset size warnings on build
  performance: {
    // Max = 300 KiB
    maxAssetSize: 307200,
    // Max = 1.5 MiB
    maxEntrypointSize: 1573500,
    // We use GIFs in the guides as mini "videos" of how to do stuff and these
    // can end up larger than our other assets so we ignore them in performance
    // output as their larger size is expected.
    //
    // We also ignore the vendors bundle (node_modules) though should keep an
    // eye on this from time to time as its pretty big too.
    assetFilter: function (assetFilename) {
      return !assetFilename.endsWith('.gif') && !assetFilename.endsWith('vendors~main.js')
    }
  },
  // main entry
  // todo - may be able to code split to be more efficient?
  entry: './src/AppRoot.jsx',
  mode: process.env.NODE_ENV,
  // devtool: 'hidden-source-map',
  // webpack loaders
  module: {
    rules: [
      // https://github.com/webpack/webpack/issues/11467
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /core-js/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            configFile: path.resolve(__dirname, 'babel.config.js'),
            compact: false,
            cacheDirectory: true,
            sourceMaps: false
          }
        }
      },
      // SCSS not loaded by components don't use CSS modules
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /styles\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      // SCSS loaded by components do use CSS modules
      {
        test: /styles\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // The relative path of the file where the current CSS is located relative to the packed root path dist
              publicPath: process.env.NODE_ENV === 'production' ? '/assets/' : '/'
            }
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[hash:8]'
              }
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(ttf|eot|png|jpe?g|gif|svg|woff?2)$/i,
        type: 'asset'
      }
    ]
  },
  resolve: {
    extensions: ['.*', '.js', '.jsx']
  },
  output: {
    path: path.join(__dirname, '/dist'),
    publicPath: process.env.NODE_ENV === 'production' ? '/assets/' : '/',
    filename: '[fullhash].[name].js'
  },
  optimization: {
    minimizer: [
      new CssMinimizerPlugin()
    ],
    splitChunks: {
      chunks: 'all'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      process: 'process/browser'
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      hash: true
    }),
    new MiniCssExtractPlugin({
      filename: '[fullhash].[name].css',
      chunkFilename: '[fullhash].[id].css'
    }),
    new webpack.DefinePlugin({
      COMMITHASH: JSON.stringify(gitRevisionPlugin.commithash()),
      STACK: JSON.stringify(process.env.STACK),
      DOMAIN: JSON.stringify(process.env.DOMAIN),
      COGNITO_CLIENT: JSON.stringify(process.env.COGNITO_CLIENT)
    }),
    new FaviconsWebpackPlugin({
      logo: './src/common/images/favicon.png',
      prefix: 'icons-[fullhash]/',
      mode: 'auto'
    })
  ],
  devServer: {
    historyApiFallback: true,
    proxy: [
      {
        context: ['/api'],
        target: 'http://localhost:3000'
      }
    ]
  }
}
