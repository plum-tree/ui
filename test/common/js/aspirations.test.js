import { assert } from 'chai'
import aspirations from '../../../src/common/js/aspirations.js'

describe('aspirations', () => {
  it('should be an array', () => {
    assert.isTrue(Array.isArray(aspirations))
  })
  it('should have no duplicate values', () => {
    assert.equal((new Set(aspirations)).size, aspirations.length)
  })
})
