import { assert } from 'chai'
import lifeStates from '../../../src/common/js/lifeStates.js'

describe('lifeStates', () => {
  it('should be an array', () => {
    assert.isTrue(Array.isArray(lifeStates))
  })
  it('should have no duplicate values', () => {
    assert.equal((new Set(lifeStates)).size, lifeStates.length)
  })
})
